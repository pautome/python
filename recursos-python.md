# Recursos Python

- prova

## Pseudocodi

- Pseudocodi gràfic i diagrames de fluxe: https://sourceforge.net/projects/pseint/

## Visualitzar execució de codi (hi ha exemples de codi, cursos)

- http://pythontutor.com/visualize.html#mode=edit

## IDE

- Thonny: IDE amb depurador pas a pas (funcions, llibreries, expressions), gestió de paquets pip, completa i coloreja codi, etc. Per iniciar-se. https://thonny.org/

- Trinket.io: Editor i python en web. https://trinket.io/

- Repl.it: Python amb depurador en web. https://repl.it

- Spyder-Anaconda: IDE Python científic (Editor, colors, debug, gràfics, profiler, ajuda, permet plugins, etc):

    - Provisión de la consola IPython (Qt) como un mensaje interactivo, que puede mostrar gráficos en línea
    - Capacidad de ejecutar fragmentos de código desde el editor de la consola
    - Análisis continuo de archivos en el editor y provisión de advertencias visuales sobre posibles errores
    - Ejecución paso a paso
    - Explorador de variables

  - https://www.spyder-ide.org/
  - https://docs.spyder-ide.org/installation.html

    ~~~
    sudo apt-get install spyder3
    ~~~

- Mini Conda: https://docs.conda.io/en/latest/miniconda.html

- Pycharm: https://www.jetbrains.com/pycharm/

## Cursos

- Curs bàsic (anglès): https://books.trinket.io/pfe/
- Curs des de 0 (gratis) i més (pagant): https://unipython.com/
- IDE educatiu Python (per professors, crear exercicis, etc): https://www.jetbrains.com/education/teaching/
- Exemples de codi: http://pp.com.mx/python/doc/ejemplos.html#requisitos
- Algorismes amb Python (avançat): https://uniwebsidad.com/libros/algoritmos-python?from=librosweb
- Algorismes amb Python (Runestone Acad): https://runestone.academy/runestone/static/pythoned/Introduction/toctree.html (en anglès http://interactivepython.org/runestone/static/pythonds/index.html)


## Ajuda i referència

- Wiki de python: https://wiki.python.org/moin/
- Aprendre python: https://data-flair.training/blogs/learn-python-notes/

## Exemples de codi

- El penjat: https://unipython.com/juego-python-ahorcado/
- python general: https://unipython.com/?s=python
- Receptes amb python: http://code.activestate.com/recipes/langs/python/
