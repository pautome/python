# Python

## Referències

- Python 3 ref - https://docs.python.org/3.7/reference/
- Python programming language (python2) - http://www.jimbrooks.org/archive/programming/python/

- https://www.tunnelsup.com/python-cheat-sheet

## Depuració

- Visualitzar execució pas a pas - https://pythontutor.com/
- Curs python pas a pas - https://futurecoder.io/

- Proves del codi: https://docs.python.org/3/library/unittest.html
- https://data-flair.training/blogs/learn-python-notes/

- PHPStan focuses on finding errors in your code without actually running it. It catches whole classes of bugs even before you write tests for the code. It moves PHP closer to compiled languages in the sense that the correctness of each line of the code can be checked before you run the actual line. - https://github.com/phpstan/phpstan

## Ethical hacking python

- Python In Ethical Hacking - https://www.simplilearn.com/tutorials/cyber-security-tutorial/python-ethical-hacking?source=sl_frs_nav_playlist_video_clicked

- https://www.hackers-arise.com/python-basics-for-hackers

## Scrap web con python

https://www.scrapingbee.com/blog/web-scraping-101-with-python/

## Recursos Awesome

- A curated list of awesome Python frameworks, libraries, software and resources - https://github.com/vinta/awesome-python
---

## tutorials

- Python Programming Language 2022 - geeksforgeeks.org - https://www.geeksforgeeks.org/python-programming-language/
- This book is a short, introductory guide for the Python programming language - https://learnbyexample.github.io/100_page_python_intro/preface.html
- Guru 99 Python Tutorial for Beginners: Learn Python Programming in 7 Days - https://www.guru99.com/python-tutorials.html
- W3schools Python - https://www.w3schools.com/python/default.asp
- Python Base de dades - https://www.w3schools.com/python/python_mysql_getstarted.asp

- Learn how to create web applications with Python and the Flask framework - https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world

How to Create Student Database Management System Using MYSQL in Python. (2022) - https://morioh.com/p/7368afb83636?s=09

## Llibres

- Llibres lliures - https://github.com/pamoroso/free-python-books
- invent with Python - http://inventwithpython.com/

## Llibreries

- Pwntools - is a CTF framework and exploit development library. Written in Python, it is designed for rapid prototyping and development, and intended to make exploit writing as simple as possible. - https://docs.pwntools.com/en/stable/index.html

- Pandas cookbook (2022) - pandas is a Python library for doing data analysis. It's really fast and lets you do exploratory work incredibly quickly. -  https://github.com/jvns/pandas-cookbook
